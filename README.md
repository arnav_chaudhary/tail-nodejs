NodeJS Tail
========

NodeJs tail is a file watcher which tails the file as per the variable filename (defult: file.log).

it returns the last n lines as per history_length(default: 3)

Assumptions
-
- Size of a default line is less than 5000 bytes.
- Port 80 is free
- fs.watch API is not valid in all OS and we may choose to run our code on that specific system

Dependencies:
-
- [Socket.io](https://socket.io/)
- 

Usage:
-
- Clone the code
- run "npm install"
- Run "node index.js"
- Connect to the server, ex: http://localhost:80



Improvements:
-
- Check if port 80 is free
- Better Exception Handling. currently we have used global handler
- parameterize application to that file path, no of lines on first display are taken via params
- better line detection with maybe higher buff size