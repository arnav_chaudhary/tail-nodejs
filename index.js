/*
Create a Tail like functionality in NodeJs
 */

var http = require('http'),
    io = require('socket.io'),
    fs = require('fs'),
    filename = "file.log",
    single_line_buff_size = 5000, //assumption here is that 20000 bytes will cover atleast 4 lines or so. can be modified if we know approx value for log size
    history_length = 3;  // decide on no of historical files to show

var history_buff_size = single_line_buff_size * history_length;


// Create Basic Node Server on port 80
// Assumption here is that port 80 is free. we can add additional checks for the same.
server = http.createServer(function(req, res){
    res.writeHead(200, {'Content-Type': 'text/html'})
    fs.readFile(__dirname + '/index.html', function(err, data){
        res.write(data, 'utf8');
        res.end();
    });
})
server.listen(80, '0.0.0.0');



// create socket
var socket = io.listen(server);
socket.on('connection', function(client){
    fs.stat(filename,function(err,stats){
        if (err) throw err;
        //get file size and read based on history buff as defined above
        var start = (stats.size > history_buff_size)?(stats.size - history_buff_size):0;
        var stream = fs.createReadStream(filename,{start:start, end:stats.size});
        stream.addListener("data", function(error_log_data){
            error_log_data = error_log_data.toString('utf-8');
            error_log_data = error_log_data.slice(error_log_data.indexOf("\n")+1).split("\n");
            error_log_data = error_log_data.slice(Math.max(error_log_data.length - history_length, 1))
            // clear log to current connected client
            client.emit('clear_log');
            // send last 3 lines to current connected client
            client.emit('log',{tail:error_log_data});
        });
    });
});


// reason to use watchfile vs watch is that watch API is not 100% consistent across platforms, and is unavailable in some situations. though we should be using watch as much as possible since it using inotify vs polling.
fs.watchFile(filename,{interval: 10}, function(curr, prev) {
    if(prev.size > curr.size) return {clear:true};
    var stream = fs.createReadStream(filename, { start: prev.size, end: curr.size});
    stream.addListener("data", function(error_log_data) {
        // send updated log to add connected clients
        socket.emit('log',{ tail : error_log_data.toString('utf-8').split("\n") });
    });
});


// catching all exceptions
process.on('uncaughtException', function (err) {
    // for logging to stdout which can be shipped
    console.log(err);
});